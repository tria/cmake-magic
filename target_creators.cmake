function(tria_executable)
    set(options          CPP11 CPP14 CPP17 CPP20 NO_COLORS EXPORT)
    set(one_value_args   TARGET NAME EXPORT_FILE EXPORT_PATH COMPONENT OUTPUT OUTPUT_DIR INSTALL_RUNTIME INSTALL_LIBRARY INSTALL_ARCHIVE)
    set(multi_value_args FILES SRC INCLUDE_DIRS INCLUDE INC LIBRARIES LIBS LIB FLAGS INSTALL)
    cmake_parse_arguments(ARG "${options}" "${one_value_args}" "${multi_value_args}" ${ARGN})

    tria_assert(ARG_TARGET "tria_executable: no TARGET specified")

    if(EXPORT_FILE OR EXPORT_PATH)
        set(ARG_EXPORT 1)
    endif()
    if (ARG_EXPORT)
        tria_pick_single(ARG_EXPORT_FILE ${ARG_TARGET} ${ARG_EXPORT_FILE})
        tria_pick_single(ARG_EXPORT_PATH "." ${ARG_EXPORT_PATH})
    endif()

    tria_pick_single(ARG_OUTPUT_DIR ${ARG_OUTPUT} ${ARG_OUTPUT_DIR})
    tria_pick_single(ARG_COMPONENT ${CMAKE_INSTALL_DEFAULT_COMPONENT_NAME} ${ARG_COMPONENT})

    tria_merge(ARG_FILES ${ARG_FILES} ${ARG_SRC})
    tria_merge(ARG_INCLUDE_DIRS ${ARG_INCLUDE_DIRS} ${ARG_INCLUDE} ${ARG_INC})
    tria_merge(ARG_LIBRARIES ${ARG_LIBRARIES} ${ARG_LIBS} ${ARG_LIB})

    tria_assert(ARG_FILES "tria_executable: no source files provided for target \"${ARG_TARGET}\"")

    add_executable(${ARG_TARGET} ${ARG_FILES})

    if(ARG_NO_COLORS)
        tria_disable_colors(${ARG_TARGET})
    elseif()
        tria_enable_colors(${ARG_TARGET})
    endif()

    if(ARG_CPP11)
        tria_enable_cpp11(${ARG_TARGET})
    endif()
    if(ARG_CPP14)
        tria_enable_cpp14(${ARG_TARGET})
    endif()
    if(ARG_CPP17)
        tria_enable_cpp17(${ARG_TARGET})
    endif()
    if(ARG_CPP20)
        tria_enable_cpp20(${ARG_TARGET})
    endif()

    if(ARG_NAME)
        tria_rename(${ARG_TARGET} ${ARG_NAME})
    endif()

    if(ARG_INCLUDE_DIRS)
        tria_include(TARGETS ${ARG_TARGET} DIRS ${ARG_INCLUDE_DIRS})
    endif()
    if(ARG_LIBRARIES)
        tria_link(TARGETS ${ARG_TARGET} LIBS ${ARG_LIBRARIES})
    endif()
    if(ARG_FLAGS)
        target_compile_options(${ARG_TARGET} PRIVATE ${ARG_FLAGS})
    endif()

    if (ARG_OUTPUT)
        tria_output_directory(TARGETS ${ARG_TARGET} RUNTIME ${ARG_OUTPUT})
    endif()

    if(ARG_INSTALL_RUNTIME)
        install(
            TARGETS ${ARG_TARGET}
            EXPORT ${ARG_EXPORT_FILE}
            COMPONENT ${ARG_COMPONENT}
            RUNTIME DESTINATION  ${ARG_INSTALL_RUNTIME}
            LIBRARY DESTINATION  ${ARG_INSTALL_LIBRARY}
            ARCHIVE DESTINATION  ${ARG_INSTALL_ARCHIVE}
        )
        if(ARG_EXPORT)
            install(EXPORT ${ARG_EXPORT_FILE} DESTINATION ${ARG_EXPORT_PATH})
        endif()
    endif()
    if(ARG_INSTALL)
        foreach(install_cmd ${ARG_INSTALL})
            separate_arguments(install_cmd)
            install(${install_cmd} COMPONENT ${ARG_COMPONENT})
        endforeach()
    endif()

endfunction()

function(tria_library)
    set(options CPP11 CPP14 CPP17 CPP20 NO_COLORS EXPORT STATIC SHARED WITH_BUILD_MODE_SYMBOL DONT_CHANGE_NAME)
    set(one_value_args TARGET NAME EXPORT_FILE EXPORT_PATH COMPONENT ENABLE_STATIC ENABLE_SHARED BUILD_MODE_SYMBOL INSTALL_LIBRARY INSTALL_ARCHIVE)
    set(multi_value_args FILES SRC INCLUDE_DIRS INCLUDE INC LIBRARIES LIBS LIB FLAGS INSTALL)
    cmake_parse_arguments(ARG "${options}" "${one_value_args}" "${multi_value_args}" ${ARGN})

    tria_assert(ARG_TARGET "tria_library: no TARGET specified")

    if(EXPORT_FILE OR EXPORT_PATH)
        set(ARG_EXPORT 1)
    endif()
    if (ARG_EXPORT)
        tria_pick_single(ARG_EXPORT_FILE ${ARG_TARGET} ${ARG_EXPORT_FILE})
        tria_pick_single(ARG_EXPORT_PATH "." ${ARG_EXPORT_PATH})
    endif()

    tria_pick_single(ARG_OUTPUT_DIR ${ARG_OUTPUT} ${ARG_OUTPUT_DIR})
    tria_pick_single(ARG_COMPONENT ${CMAKE_INSTALL_DEFAULT_COMPONENT_NAME} ${ARG_COMPONENT})

    tria_merge(ARG_FILES ${ARG_FILES} ${ARG_SRC})
    tria_merge(ARG_INCLUDE_DIRS ${ARG_INCLUDE_DIRS} ${ARG_INCLUDE} ${ARG_INC})
    tria_merge(ARG_LIBRARIES ${ARG_LIBRARIES} ${ARG_LIBS} ${ARG_LIB})

    tria_pick_single(ARG_INSTALL_LIBRARY {ARG_INSTALL_ARCHIVE} ${ARG_INSTALL_LIBRARY})
    tria_pick_single(ARG_INSTALL_ARCHIVE {ARG_INSTALL_LIBRARY} ${ARG_INSTALL_ARCHIVE})

    tria_pick_single(ARG_NAME ${ARG_TARGET} ${ARG_NAME})

    string(TOUPPER ${ARG_TARGET}_BUILD_MODE BUILD_MODE_SYMBOL)
    tria_pick_single(BUILD_MODE_SYMBOL ${BUILD_MODE_SYMBOL} ${ARG_BUILD_MODE_SYMBOL})

    if(EXPORT_FILE OR EXPORT_PATH)
        set(ARG_EXPORT 1)
    endif()
    if (ARG_EXPORT)
        tria_pick_single(ARG_EXPORT_FILE ${ARG_TARGET} ${ARG_EXPORT_FILE})
        tria_pick_single(ARG_EXPORT_PATH "." ${ARG_EXPORT_PATH})
    endif()

    set(BUILD_STATIC FALSE)
    set(BUILD_SHARED FALSE)
    if (ARG_STATIC OR ARG_ENABLE_STATIC)
        set(BUILD_STATIC TRUE)
    endif()
    if (ARG_SHARED OR ARG_ENABLE_SHARED)
        set(BUILD_SHARED TRUE)
    endif()

    if(NOT BUILD_STATIC AND NOT BUILD_SHARED AND ARG_FILES)
        set(BUILD_STATIC TRUE)
    endif()

    set(TARGETS "")
    set(MULTI_BUILD_MODE FALSE)
    if(BUILD_STATIC AND BUILD_SHARED)
        set(MULTI_BUILD_MODE TRUE)
        set(TARGETS "${ARG_TARGET}_static" "${ARG_TARGET}_shared")

        add_library(${ARG_TARGET} OBJECT ${ARG_FILES})
        set_property(TARGET ${ARG_TARGET} PROPERTY POSITION_INDEPENDENT_CODE 1)

        add_library(${ARG_TARGET}_static STATIC $<TARGET_OBJECTS:${ARG_TARGET}>)
        add_library(${ARG_TARGET}_shared SHARED $<TARGET_OBJECTS:${ARG_TARGET}>)
        if(NOT ARG_DONT_CHANGE_NAME)
            set_target_properties(${ARG_TARGET}_static PROPERTIES OUTPUT_NAME ${ARG_NAME})
            set_target_properties(${ARG_TARGET}_shared PROPERTIES OUTPUT_NAME ${ARG_NAME})
        endif()

    elseif(BUILD_STATIC OR BUILD_SHARED)
        set(TARGETS "${ARG_TARGET}")

        if(BUILD_STATIC)
            add_library(${ARG_TARGET} STATIC ${ARG_FILES})
        else()
            add_library(${ARG_TARGET} SHARED ${ARG_FILES})
        endif()

        if(NOT ARG_DONT_CHANGE_NAME)
            set_target_properties(${ARG_TARGET} PROPERTIES OUTPUT_NAME ${ARG_NAME})
        endif()

    else()
        add_library(${ARG_TARGET} INTERFACE)

    endif()

    if(ARG_INCLUDE_DIRS)
        tria_include(TARGETS ${ARG_TARGET} DIRS ${ARG_INCLUDE_DIRS})
    endif()

    if(BUILD_STATIC OR BUILD_SHARED)
        if(ARG_NO_COLORS)
            tria_disable_colors(${ARG_TARGET})
        else()
            tria_enable_colors(${ARG_TARGET})
        endif()

        if(ARG_WITH_BUILD_MODE_SYMBOL OR ARG_BUILD_MODE_SYMBOL)
            target_compile_definitions(${ARG_TARGET} PUBLIC -D${BUILD_MODE_SYMBOL})
        endif()

        if(ARG_CPP11)
            tria_enable_cpp11(${ARG_TARGET})
        endif()
        if(ARG_CPP14)
            tria_enable_cpp14(${ARG_TARGET})
        endif()
        if(ARG_CPP17)
            tria_enable_cpp17(${ARG_TARGET})
        endif()
        if(ARG_CPP20)
            tria_enable_cpp20(${ARG_TARGET})
        endif()

        if(ARG_LIBRARIES)
            tria_link(TARGETS ${ARG_TARGET} LIBS ${ARG_LIBRARIES})
        endif()
        if(ARG_FLAGS)
            target_compile_options(${ARG_TARGET} PRIVATE ${ARG_FLAGS})
        endif()

        if(ARG_INSTALL_LIBRARY OR ARG_INSTALL_ARCHIVE)
            install(
                TARGETS ${TARGETS}
                EXPORT ${ARG_EXPORT_FILE}
                COMPONENT ${ARG_COMPONENT}
                LIBRARY DESTINATION ${ARG_INSTALL_LIBRARY}
                ARCHIVE DESTINATION ${ARG_INSTALL_ARCHIVE}
            )
            if(ARG_EXPORT)
                install(EXPORT ${ARG_EXPORT_FILE} DESTINATION ${ARG_EXPORT_PATH})
            endif()
        endif()
    endif()

    if(ARG_INSTALL)
        foreach(install_cmd ${ARG_INSTALL})
            separate_arguments(install_cmd)
            install(${install_cmd} COMPONENT ${ARG_COMPONENT})
        endforeach()
    endif()

endfunction()