# tria/cmake-magic

Want to use cmake, but that CMakeFiles looks so weird? Where is handy things, why I should do all that trash with so much LOC all the time?

Make your dreams come true! Simply start using our cmake magic.

- Use your weapon right and don't shoot allies. We use only target-specific options and no global redefinitions.
- Be the God of build system, do hard things in one line.
- Oh, we have 500 targets with same includes and libraries, we don't want 1000 LOC, we want 2 (yes you can).
- Static or dynamic library? Maybe, header-only. Don't choose, build them all (or not).

## How to enable

- As git submodule:

```
git submodule add https://gitlab.com/tria/cmake-magic [path/to/folder]
```

- As simple folder:

```
git clone https://gitlab.com/tria/cmake-magic [path/to/folder]
```

And then in CMakelists.txt:

```
add_subdirectory(path/to/folder)
```

## Magic overview

### Global stuff

```
tria_enable_ccache()
```

Enables usage of ccache, if possible. Warning - this operation will override `CMAKE_COMPILER_LAUNCHER` and `CMAKE_LINKER_LAUNCHER` for both C and C++ languages and will work only if that variables are empty.

```
tria_enable_colors(targets...)
```

Enables colorful diagnostics output (gcc, clang) for selected targets.

### Creating targets

#### Common things

- `NAME` - overrides binary file name

- `CPP11`, `CPP14`, `CPP17` - enables C++11, C++14 or C++17 accordingly

- `FILES` - files (translation units) to build

- `INCLUDE_DIRS` - paths with include directories (like target_include_directories)

- `LIBS` - dependency libraries or targets (like target_link_libraries)


#### Library

```
tria_library(
    TARGET target
    FILES files
    [STATIC | ENABLE_STATIC bool]
    [SHARED | ENABLE_SHARED bool]
    [INCLUDE_DIRS / INCLUDES inlude_dirs]
    [LIBRARIES / LIBS libs]
    [NAME name]                # change binary name (target -> name)
    [CPP11] [CPP14] [CPP17]    # enable C++11, C++14 or C++17
    [DONT_CHANGE_NAME]         # turn off name change for binaries (like target_static -> target)
    [WITH_BUILD_MODE_SYMBOL]   # creates symbol `upper(target)_BUILD_MODE`
    [BUILD_MODE_SYMBOL symbol] # override symbol name from WITH_BUILD_MODE_SYMBOL
)
```

Creates library target `target` with files, includes and libraries, if provided. You can choose linkage type with STATIC or SHARED flags, or with boolean parameters ENABLE_STATIC and ENABLE SHARED. You can use them together and build both static and shared versions. If no mode specified, it's like header-only "build".

When in multi build mode (both shared and static build enabled) there is some features. Static target will be named `target_static`, and shared one will be named `target_shared`. But dy default output names for binaries will be renamed to match `target`. You can disable it with `DONT_CHANGE_NAME`.

If you use "our" style to accomplish header-only + static + shared concept (or maybe you just want it), you can provide `BUILD_MODE_SYMBOL`. That symbol will be added, if static or shared version was chosen. Or you can provide `WITH_BUILD_MODE_SYMBOL`, and it will be `target_BUILD_MODE`.

#### Executable

```
tria_executable(
    TARGET target
    FILES files
    [INCLUDE_DIRS / INCLUDES inlude_dirs]
    [LIBRARIES / LIBS libs]
    [NAME name]                # change binary name (target -> name)
    [CPP11] [CPP14] [CPP17]    # enable C++11, C++14 or C++17
)
```

### Target manupulations

```
tria_include(TARGETS targets INCLUDE_DIRS includes)
tria_link(TARGETS targets LIBS libs)
tria_output_directory(TARGETS targets ARCHIVE path LIBRARY path RUNTIME path)
tria_rename(target name)
tria_enable_cpp11(targets)
tria_enable_cpp14(targets)
tria_enable_cpp17(targets)
```

### Misc

#### Code coverage

```
tria_enable_coverage(
    TARGETS targets
    EXEC test
    [DEPS targets]
    [EXCLUDES excludes]
)
```

#### heaptrack support

```
tria_enable_heaptrack(
    FOR target
    [TARGET_NAME name]        # target for make, default is "heaptrack"
    [RESULT file]             # heaptrack result file without extension, default is "heaptrack.$target"
    [FLAME / FLAMEGRAPH file] # generate flamegraph
    [HIST / HISTOGRAM file]   # generate histogram
    [MASSIF file]             # generate massif file
    [PEAKS] [LEAKS]
    [ALLOCS / ALLOCATORS]
    [TEMPORARY / TEMPS]
    [SHORTEN_TEMPLATES / SHORT_TPLS]
    [MERGE_BACKTRACES] [PRINT_REPORT] [DONT_SAVE_REPORT]
)
```
