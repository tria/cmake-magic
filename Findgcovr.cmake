include(FindPackageHandleStandardArgs)

find_program(
	GCOVR gcovr
	HINTS ${GCOVR_ROOT}
		 "${GCOVR_ROOT}/bin"
		 "${GCOVR_ROOT}/share/python"
		 "/usr/share/python"
		 "/usr/local/share/python"
		 "${CMAKE_INSTALL_PREFIX}/share/python"
)

set(GCOVR_FOUND FALSE CACHE INTERNAL "" PARENT_SCOPE)

find_package_handle_standard_args(
	gcovr
	FOUND_VAR GCOVR_FOUND
	REQUIRED_VARS GCOVR
)

mark_as_advanced(GCOVR GCOVR_FOUND)