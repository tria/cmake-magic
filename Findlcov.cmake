include(FindPackageHandleStandardArgs)

find_program(LCOV    lcov)
find_program(GENHTML genhtml)

set(LCOV_FOUND FALSE CACHE INTERNAL "" PARENT_SCOPE)

find_package_handle_standard_args(
    lcov
    FOUND_VAR LCOV_FOUND
    REQUIRED_VARS LCOV GENHTML
)

mark_as_advanced(LCOV GENHTML LCOV_FOUND)