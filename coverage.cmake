include(CMakeParseArguments)

find_package(lcov)
find_package(gcovr)

function(tria_enable_coverage)

    set(options "")
    set(oneValueArgs EXEC)
    set(multiValueArgs TARGETS DEPS EXCLUDES)
    cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    if(NOT LCOV)
        message(STATUS "lcov and / or genhtml not found! Skipping code coverage...")
        return()
    endif() # NOT LCOV_PATH

    if(NOT GCOVR)
        message(STATUS "gcovr not found! Skipping code coverage...")
    endif()

    foreach(target ${ARG_TARGETS})
        if(CMAKE_COMPILER_IS_GNUCXX)
            target_link_libraries(${target} gcov)
        else()
            target_compile_options(${target} PUBLIC --coverage)
        endif()
    endforeach()
    tria_set_coverage_flags(${ARG_TARGETS})

    set(EXCLUDES "")
    foreach(exclude ${ARG_EXCLUDES})
        set(EXCLUDES "${EXCLUDES};'${exclude}'")
    endforeach()

    add_custom_target(coverage
        # Cleanup lcov
        COMMAND ${LCOV} --directory . --zerocounters

        COMMAND ${ARG_EXEC}

        # Capturing lcov counters and generating report
        COMMAND ${LCOV} -d ${CMAKE_BINARY_DIR} -d ${CMAKE_SOURCE_DIR} --no-external --capture --output-file coverage.info
        COMMAND ${LCOV} --remove coverage.info ${EXCLUDES} --output-file coverage.info
        COMMAND ${GENHTML} -o coverage --demangle-cpp coverage.info
        COMMAND ${CMAKE_COMMAND} -E remove coverage.info coverage.info

        WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
        DEPENDS ${ARG_DEPS}
        COMMENT "Resetting code coverage counters to zero.\nProcessing code coverage counters and generating report."
    )

    # Show info where to find the report
    add_custom_command(TARGET coverage POST_BUILD
        COMMAND ;
        COMMENT "Open ./coverage/index.html in your browser to view the coverage report."
    )

endfunction()

function(tria_set_coverage_flags)
    foreach(target ${ARGV})
        target_compile_options(${target} PRIVATE -g -O0 --coverage -ftest-coverage -fprofile-arcs -ftest-coverage -fno-inline -fno-inline-small-functions -fno-default-inline)
    endforeach()
    string(REPLACE ";" ", " _ARGV "${ARGV}")
    message(STATUS "${_ARGV}: appending code coverage compiler flags: -g -O0 --coverage -fprofile-arcs -ftest-coverage")
endfunction()